import { join } from 'path'
import { pathExists } from 'fs-extra'
import * as ffmpeg from 'fluent-ffmpeg'

export type VideoTranscodingFPS = {
  MIN: number,
  AVERAGE: number,
  MAX: number,
  KEEP_ORIGIN_FPS_RESOLUTION_MIN: number
}

export enum VideoResolution {
  H_240P = 240,
  H_360P = 360,
  H_480P = 480,
  H_720P = 720,
  H_1080P = 1080
}

export const VIDEO_TRANSCODING_FPS = {
  MIN: 30,
  MAX: 60,
  AVERAGE: 30,
  KEEP_ORIGIN_FPS_RESOLUTION_MIN: 30
} as VideoTranscodingFPS

/**
 * Bitrate targets for different resolutions, at VideoTranscodingFPS.AVERAGE.
 *
 * Sources for individual quality levels:
 * Google Live Encoder: https://support.google.com/youtube/answer/2853702?hl=en
 * YouTube Video Info (tested with random music video): https://www.h3xed.com/blogmedia/youtube-info.php
 */
function getBaseBitrate (resolution: VideoResolution) {
  switch (resolution) {
  case VideoResolution.H_240P:
    // quality according to Google Live Encoder: 300 - 700 Kbps
    // Quality according to YouTube Video Info: 186 Kbps
    return 250 * 1000
  case VideoResolution.H_360P:
    // quality according to Google Live Encoder: 400 - 1,000 Kbps
    // Quality according to YouTube Video Info: 480 Kbps
    return 500 * 1000
  case VideoResolution.H_480P:
    // quality according to Google Live Encoder: 500 - 2,000 Kbps
    // Quality according to YouTube Video Info: 879 Kbps
    return 900 * 1000
  case VideoResolution.H_720P:
    // quality according to Google Live Encoder: 1,500 - 4,000 Kbps
    // Quality according to YouTube Video Info: 1752 Kbps
    return 1750 * 1000
  case VideoResolution.H_1080P: // fallthrough
  default:
    // quality according to Google Live Encoder: 3000 - 6000 Kbps
    // Quality according to YouTube Video Info: 3277 Kbps
    return 3300 * 1000
  }
}

/**
 * Calculate the target bitrate based on video resolution and FPS.
 *
 * The calculation is based on two values:
 * Bitrate at VideoTranscodingFPS.AVERAGE is always the same as
 * getBaseBitrate(). Bitrate at VideoTranscodingFPS.MAX is always
 * getBaseBitrate() * 1.4. All other values are calculated linearly
 * between these two points.
 */
export function getTargetBitrate (resolution: VideoResolution, fps: number, fpsTranscodingConstants: VideoTranscodingFPS) {
  const baseBitrate = getBaseBitrate(resolution)
  // The maximum bitrate, used when fps === VideoTranscodingFPS.MAX
  // Based on numbers from Youtube, 60 fps bitrate divided by 30 fps bitrate:
  //  720p: 2600 / 1750 = 1.49
  // 1080p: 4400 / 3300 = 1.33
  const maxBitrate = baseBitrate * 1.4
  const maxBitrateDifference = maxBitrate - baseBitrate
  const maxFpsDifference = fpsTranscodingConstants.MAX - fpsTranscodingConstants.AVERAGE
  // For 1080p video with default settings, this results in the following formula:
  // 3300 + (x - 30) * (1320/30)
  // Example outputs:
  // 1080p10: 2420 kbps, 1080p30: 3300 kbps, 1080p60: 4620 kbps
  //  720p10: 1283 kbps,  720p30: 1750 kbps,  720p60: 2450 kbps
  return baseBitrate + (fps - fpsTranscodingConstants.AVERAGE) * (maxBitrateDifference / maxFpsDifference)
}

/**
 * The maximum bitrate we expect to see on a transcoded video in bytes per second.
 */
export function getMaxBitrate (resolution: VideoResolution, fps: number, fpsTranscodingConstants: VideoTranscodingFPS) {
  return getTargetBitrate(resolution, fps, fpsTranscodingConstants) * 2
}


function getVideoFileStream (path: string) {
  return new Promise<any>((res, rej) => {
    ffmpeg.ffprobe(path, (err, metadata) => {
      if (err) return rej(err)

      const videoStream = metadata.streams.find(s => s.codec_type === 'video')
      if (!videoStream) throw new Error('Cannot find video stream of ' + path)

      return res(videoStream)
    })
  })
}

function getAudioStream (option: ffmpeg.FfmpegCommand | string) {
  // without position, ffprobe considers the last input only
  // we make it consider the first input only
  // if you pass a file path to pos, then ffprobe acts on that file directly
  return new Promise<{ absolutePath: string, audioStream?: any }>((res, rej) => {

    function parseFfprobe (err: any, data: ffmpeg.FfprobeData) {
      if (err) return rej(err)

      if ('streams' in data) {
        const audioStream = data.streams.find(stream => stream['codec_type'] === 'audio')
        if (audioStream) {
          return res({
            absolutePath: data.format.filename,
            audioStream
          })
        }
      }

      return res({ absolutePath: data.format.filename })
    }

    if (typeof option === 'string') {
      return ffmpeg.ffprobe(option, parseFfprobe)
    }

    return option.ffprobe(parseFfprobe)
  })
}

async function getVideoFileFPS (path: string) {
  const videoStream = await getVideoFileStream(path)

  for (const key of [ 'r_frame_rate' , 'avg_frame_rate' ]) {
    const valuesText: string = videoStream[key]
    if (!valuesText) continue

    const [ frames, seconds ] = valuesText.split('/')
    if (!frames || !seconds) continue

    const result = parseInt(frames, 10) / parseInt(seconds, 10)
    if (result > 0) return Math.round(result)
  }

  return 0
}

async function getVideoFileResolution (path: string) {
  const videoStream = await getVideoFileStream(path)

  return {
    videoFileResolution: Math.min(videoStream.height, videoStream.width),
    isPortraitMode: videoStream.height > videoStream.width
  }
}

async function getVideoFileBitrate (path: string) {
  return new Promise<number>((res, rej) => {
    ffmpeg.ffprobe(path, (err, metadata) => {
      if (err) return rej(err)

      return res(metadata.format.bit_rate)
    })
  })
}

async function generateHighBitrateVideo () {
  const tempFixturePath = join(__dirname, 'video_high_bitrate_1080p.mp4')

  const exists = await pathExists(tempFixturePath)
  if (!exists) {

    // Generate a random, high bitrate video on the fly, so we don't have to include
    // a large file in the repo. The video needs to have a certain minimum length so
    // that FFmpeg properly applies bitrate limits.
    // https://stackoverflow.com/a/15795112
    return new Promise<string>(async (res, rej) => {
      ffmpeg()
        .outputOptions([ '-f rawvideo', '-video_size 1920x1080', '-i /dev/urandom' ])
        .outputOptions([ '-ac 2', '-f s16le', '-i /dev/urandom', '-t 10' ])
        .outputOptions([ '-maxrate 10M', '-bufsize 10M' ])
        .output(tempFixturePath)
        .on('error', rej)
        .on('end', () => res(tempFixturePath))
        .run()
    })
  }

  return tempFixturePath
}

/* ----------------------------------------------------------------------- */

export {
  getAudioStream,
  getVideoFileStream,
  getVideoFileFPS,
  getVideoFileResolution,
  getVideoFileBitrate,
  generateHighBitrateVideo
}
