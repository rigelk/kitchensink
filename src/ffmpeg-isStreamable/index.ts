import {
  getAudioStream,
  getVideoFileStream,
  getVideoFileFPS,
  getVideoFileResolution,
  getVideoFileBitrate,
  getMaxBitrate,
  VIDEO_TRANSCODING_FPS
} from './ffmpeg-utils.shim'
import { execPromise } from '../helpers'

export default async function isVideoFileStreamable (path: string) {
  /* testing codecs */
  const isAVC = await getVideoFileStream(path).then(s => {
    return s['codec_name'] === 'h264'
  })
  const isAAC = await getAudioStream(path).then(s => {
    return s.audioStream['codec_name'] === 'aac'
  })

  /* testing flags */
  const hasFastStart = await new Promise<boolean>(async (res, rej) => {
    try {
      const command = await execPromise(`ffmpeg -i subfile,,start,0,end,10000,,:${path} -v 56 2>&1 | grep -e 
      "type:'moov'" -e "type:'mdat'" | head -1`)
      res( command.indexOf('moov') !== -1 ) // true if we find the moov
    } catch (err) {
      rej(err)
    }
  })

  /* testing bitrate */
  const bitrate = await getVideoFileBitrate(path)
  const hasBitrateInRange = bitrate < getMaxBitrate(
    (await getVideoFileResolution(path)).videoFileResolution,
    (await getVideoFileFPS(path)),
    VIDEO_TRANSCODING_FPS
  )

  return {
    hasBitrateInRange,
    hasFastStart,
    isAVC,
    isAAC
  }
}
