/* modules */
var ffmpeg_command = require('fluent-ffmpeg')
import {
  VideoResolution,
  getVideoFileBitrate,
  getMaxBitrate,
  generateHighBitrateVideo,
  VIDEO_TRANSCODING_FPS
} from './ffmpeg-utils.shim'
import isVideoFileStreamable from './index'

/* test modules */
var mlog = require('mocha-logger')
import { fixture, execPromise } from '../helpers'
import { expect, assert } from 'chai'
import 'mocha'
import { mkdirp, rmdir } from 'fs-extra'

describe('Checks codecs and faststart in videos', () => {

  before(() => {
    mkdirp('tmp')
  })

  /**
   * Naive version: first draft to prove the information can be obtained
   */
  it('should detect lack of faststart using fluent-ffmpeg [NAIVE IMPLEMENTATION]', (done) => {
    // sadly the command doesn't support taking a stream using only the first bytes
    // of the file, so we have to go through the whole file
    ffmpeg_command(fixture('video_short.mp4'), { stdoutLines: 0 })
      .inputOption('-v 56')
      .on('end', (stdout: any) => {
        const moov = stdout.indexOf('type:\'moov\'') < stdout.indexOf('type:\'mdat\'')
        mlog.log('MOOV is streamable?', moov)
        expect(moov, 'MOOV should _not_ be present with this fixture').to.equal(false)
        done()
      })
      .save('tmp/test.mp4') // this is the main problem: to access stdout, we have to write the file somewhere
  })

  /**
   * Faster version: using direct subprocess of ffmpeg, because sadly fluent-ffmpeg
   * cannot deal with the subfile ffmpeg protocol which allows constant-time checking
   */
  it('should detect lack of faststart using ffmpeg subprocess [DIRTY BUT OPTIMISED]', async () => {
    const fixturePath = fixture('video_short.mp4')

    try {
      const command = await execPromise(`ffmpeg -i subfile,,start,0,end,10000,,:${fixturePath} -v 56 2>&1 | grep -e 
      "type:'moov'" -e "type:'mdat'" | head -1`)
      const moov = command.indexOf('moov') !== -1 // true if we find the moov
      expect(moov).to.equal(false)
    } catch (err) {
      assert.fail("The ffmpeg command failed for some reason.")
    }
  })

  /**
   * Engineered version: wraps up faststart check with other verifications
   */
  it('should detect a proper .mp4 as AAC, AVC with no faststart', async () => {
    const report = await isVideoFileStreamable(fixture('video_short.mp4'))

    expect(report.isAVC).to.equal(true)
    expect(report.isAAC).to.equal(true)
    expect(report.hasBitrateInRange).to.equal(true)
    expect(report.hasFastStart).to.equal(false)
  })

  after((done) => {
    rmdir('tmp', () => done())
  })

})

describe('Checks bitrate corresponds to the resolution/fps in videos', () => {

  before(() => {
    mkdirp('tmp')
  })

  it('should detect very high bitrates', async () => {
    mlog.log('generating a high bitrate video… this could take a while.')
    let tempFixturePath: string
    {
      tempFixturePath = await generateHighBitrateVideo()

      const bitrate = await getVideoFileBitrate(tempFixturePath)
      expect(bitrate).to.be.above(getMaxBitrate(VideoResolution.H_1080P, 60, VIDEO_TRANSCODING_FPS))
    }
    
    const report = await isVideoFileStreamable(tempFixturePath)
    expect(report.hasBitrateInRange).to.equal(false)
  }).timeout(160000)

  after((done) => {
    rmdir('tmp', () => done())
  })

})