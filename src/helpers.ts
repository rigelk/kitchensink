import { isAbsolute, join } from 'path'
import { exec } from 'child_process'

function root () {
  const paths = [ __dirname ]

  // We are under /dist directory
  if (process.mainModule && process.mainModule.filename.endsWith('.ts') === false) {
    paths.push('..')
  }

  return join.apply(null, paths)
}

function fixture (name: string) {
  return join(root(), 'includes/PeerTube/server/tests/fixtures/', name)
}

function buildAbsoluteFixturePath (path: string, customTravisPath = false) {
  if (isAbsolute(path)) {
    return path
  }

  if (customTravisPath && process.env.TRAVIS) return join(process.env.HOME, 'fixtures', path)

  return join(__dirname, '..', 'includes', 'PeerTube', 'server', 'tests', 'fixtures', path)
}

/* promise utils */
// Thanks to https://gist.github.com/kumasento/617daa7e46f13ecdd9b2
function promisify1<T, A> (func: (arg: T, cb: (err: any, result: A) => void) => void): (arg: T) => Promise<A> {
  return function promisified (arg: T): Promise<A> {
    return new Promise<A>((resolve: (arg: A) => void, reject: (err: any) => void) => {
      func.apply(null, [ arg, (err: any, res: A) => err ? reject(err) : resolve(res) ])
    })
  }
}
const execPromise = promisify1<string, string>(exec)

// ---------------------------------------------------------------------------------------

export {
  root,
  fixture,
  buildAbsoluteFixturePath,
  execPromise
}