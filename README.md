# Kitchen Sink for PeerTube

This is testbed for functions that cannot be pushed to PeerTube mainline but would still
benefit from public input until that is possible. Think of it as a way to battle-test
functions by testing them independently and using a lightweight version of the PeerTube
testing suite.

It is especially useful to share bits of code for others to take, especially when
big refactoring is needed for a whole feature to be written. Just having to take and
connect functions that have already proven their worth against test cases make writing
more complex features easier.

## Usage

`yarn install`

`npm run test:all` to run all test cases, for each project in `./src`.

## Code organisation

The `./src` directory holds all code snippets, each in their own project folder,
each being able to install their own dependencies in their own `package.json`.

You can use you own test suite, but beware that `mocha` will try to run
all `*.spec.ts` definitions.

`./src/helper.ts` holds common file helpers, especially to get fixtures out
of PeerTube.
